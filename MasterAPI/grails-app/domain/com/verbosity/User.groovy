package com.verbosity
import masterapi.*

class User {

	String username
	String passwordHash

	static hasMany = [ roles: Role, permissions: String , messages: Messages, examples:Example]

	static constraints = {
		username(nullable: false, blank: false, unique: true)
	}
}
