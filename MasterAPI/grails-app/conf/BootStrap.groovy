import com.verbosity.Role
import com.verbosity.User
import org.apache.shiro.crypto.hash.Sha256Hash

class BootStrap {

	def init = { servletContext ->

		def user1 = new User(username: "user123", passwordHash: new Sha256Hash("p").toHex())
		user1.addToPermissions("*:*")
		user1.save()
		
		
		Role userRole = new Role(name: "ROLE_USER").save(failOnError: true)
		userRole.addToPermissions("*:*")
		userRole.save(failonError: true)
		Role adminRole = new Role(name: "ROLE_ADMIN").save(failOnError: true)
		adminRole.addToPermissions("*:*")
		adminRole.save(failonError: true)
		
		
	}
	def destroy = {
	}
}
