package com.verbosity


import com.verbosity.User
import grails.converters.JSON
import org.apache.shiro.SecurityUtils
import org.apache.shiro.authc.AuthenticationException
import org.apache.shiro.authc.UsernamePasswordToken
import org.apache.shiro.crypto.hash.Sha256Hash

class LoginController {

	def responseUserCreated = [
		'results': "user created",
		'status': "true"
	]

	def responseUserNotCreated = [
		'results': "user not created, user param missing",
		'status': "false"
	]

	String mUser;

	// create user and login
	def createUser() {

		if(!params.user)
			render responseUserNotCreated as JSON;
		else {
			mUser = params.user as String;
			String cryptedUser = new Sha256Hash(mUser).toHex();

			User user1 = new User(username: mUser, passwordHash: cryptedUser)
			user1.addToPermissions("*:*")
			user1.save()

			login(mUser)


		}
	}

	// just login
	def login(String createdUser) {

		if(!createdUser)
			mUser = params.user as String;

		def authToken = new UsernamePasswordToken(mUser, mUser);

		authToken.rememberMe = true

		try{
			// Perform the actual login. An AuthenticationException
			// will be thrown if the username is unrecognised or the
			// password is incorrect.
			SecurityUtils.subject.login(authToken)

			render responseUserCreated as JSON;
		}
		catch (AuthenticationException ex){
			render "Auth Error $ex";
		}
	}
}
