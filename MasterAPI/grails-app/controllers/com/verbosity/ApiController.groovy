package com.verbosity

import grails.converters.JSON
import masterapi.Example

import io.netty.bootstrap.Bootstrap;
import org.apache.shiro.SecurityUtils
import org.apache.shiro.authc.AuthenticationException
import org.apache.shiro.authc.UsernamePasswordToken
import org.apache.shiro.crypto.hash.Sha256Hash
import org.apache.shiro.session.Session
import org.apache.shiro.subject.Subject

class ApiController {

	def shiroSecurityService

	def scaffold = Messages
	//def shiroSecurityManager

	def responseMessageCreated = [
		'results': "message created",
		'status': "true"
	]

	def responseMessageNotCreated = [
		'results': "message not created, user param missing",
		'status': "false"
	]

	def responseUserCreated = [
		'results': "user created",
		'status': "true"
	]

	def responseUserLoggedin = [
		'results': "user logged in",
		'status': "true"
	]

	def responseUserNotCreated = [
		'results': "user not created, user param missing",
		'status': "false"
	]

	//def messages = principal ? Messages.findAllByContent(principal) : []


	def index()
	{
		 Bootstrap b = new Bootstrap();
		User user = authUser();
	
		
		Example example = new Example (mystuff: "Crap", createdOn: new Date())
		
		user.addToExamples(example)


		if (!example.save(flush:true)){
			log.info "Could not save msg!!"
			log.info "${example.errors}"
		}

		render "Hello"
	}

	private User authUser() {
		Subject currentUser = SecurityUtils.getSubject();
		Session session = currentUser.getSession();
		def principal = currentUser.getPrincipal() as String;

		User user = User.findByUsername(principal)
		return user
	}

	def	 getmessages()
	{

		Subject currentUser = SecurityUtils.getSubject();
		Session session = currentUser.getSession();
		def principal = currentUser.getPrincipal() as String;
		User user = User.findByUsername(principal);

		user.messages*.content;

		println "Hi Nick!"

		render user.messages*.content as JSON;

	}


	def message() {

		/* receive json in format '{"user_id":1, "message":"great place"}' */

		if(!request.JSON.message)
			render responseMessageNotCreated as JSON;
		else {

			Subject currentUser = SecurityUtils.getSubject();
			Session session = currentUser.getSession();
			def principal = currentUser.getPrincipal() as String;

			User user = User.findByUsername(principal);

			Messages msg = new Messages(content: request.JSON.message, createdOn: new Date());
			user.addToMessages(msg);


			if (!user.save(flush:true)){
				log.info "Could not save msg!!"
				log.info "${user.errors}"
			}

			render responseMessageCreated as JSON;
		}
	}

	String mUser;

	// create user and login
	def createUser() {

		if(!params.user)
			render responseUserNotCreated as JSON;
		else {

			mUser = params.user as String;
			String cryptedUser = new Sha256Hash(mUser).toHex();

			User newUser = new User(username: mUser, passwordHash: cryptedUser)

			newUser.addToPermissions("*:*")

			newUser.save(flush:true)

			login(mUser)


		}
	}

	// just login
	def login(String createdUser) {

		def response = responseUserCreated;

		if(!createdUser) {
			mUser = params.user as String;
			response = responseUserLoggedin;
		}

		log.info "using mUser as $mUser"

		def authToken = new UsernamePasswordToken(params.user, params.user as String)

		try{
			// Perform the actual login. An AuthenticationException
			// will be thrown if the username is unrecognised or the
			// password is incorrect.
			SecurityUtils.subject.login(authToken)
			def subject = SecurityUtils.getSubject();
			log.info "Logging in user $createdUser or $mUser"
			render response as JSON;
		}
		catch (AuthenticationException ex){
			render "Auth Error $ex";
		}
	}

}